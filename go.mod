module gitlab.com/lightmeter/controlcenter

go 1.14

require (
	github.com/hpcloud/tail v1.0.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd // indirect
	gitlab.com/lightmeter/postfix-log-parser v0.0.0-20200610135947-b920cac03ef4
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
